n = int(input())
data = list(map(int, input().split()))
data.sort()

cnt = 0    # 모험가 수
result = 0    # 그룹의 수

for i in data:    # 모험가 리스트를 반복
    cnt += 1      # 모험가를 포함
    if cnt >= i:  # 모험가 수가 공포도보다 크거나 같을 때
        result += 1    # 여행을 떠남
        cnt = 0        # 모험가 수 초기화

print(result)