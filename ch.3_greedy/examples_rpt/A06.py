# heap (min)

import heapq

def solution(food_times, k):
    if sum(food_times) <= k:
        return -1

    heap = []
    heapq.heapify(heap)

    for i in range(len(food_times)):
        heapq.heappush(heap, (food_times[i], i+1))

    sum_value = 0    # 먹기 위해 사용한 시간
    previous = 0    # 직전에 다 먹은 음식 시간
    length = len(food_times)    # 남은 음식의 개수

    while sum_value + ((heap[0][0] - previous) * length) <= k:
        now = heapq.heappop(heap)[0]
        sum_value += (now-previous) * length
        length -= 1
        previous = now

    result = sorted(heap, key=lambda x:x[1])
    return result[(k-sum_value) % length]


food_times = [3, 1, 2]
k = 5
print(solution(food_times, k))