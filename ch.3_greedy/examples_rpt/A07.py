s = str(input())
n = int(len(s)/2)

left = s[:n]
right = s[n:]

print(left, right)

summary = 0
for i in left:
    summary += int(i)

for j in right:
    summary -= int(j)

if summary != 0:
    print("READY")
else:
    print("LUCKY")