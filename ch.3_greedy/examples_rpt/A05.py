from itertools import combinations
import sys

a, b = list(map(int, sys.stdin.readline().split()))
data = list(map(int, sys.stdin.readline().split()))

l = [0] * 11
for x in data:
    l[x] += 1

result = 0
for i in range(1, b+1):
    a -= l[i]
    result += l[i] * b

print(result)