n, m, k = map(int, input().split())
data = list(map(int, input().split()))
data.sort()

result = 0

init_k = k
for _ in range(m):
    if k != 0:
        result += data[-1]
        k -= 1
    else:
        result += data[-2]
        k = init_k


print(result)

# k번째 수가 아니면
# 리스트에서 제일 큰 값을 더하고,
# k의 배수이면
# 리스트에서 두번째로 큰 값을 더한다