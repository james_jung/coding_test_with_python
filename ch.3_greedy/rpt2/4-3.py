place = input() # a1

row = int(place[1])
column = int(ord(place[0])) - int(ord('a')) + 1

steps = [(-2, -1), (-2, 1), (-1, -2), (-1, 2), (1, -2), (1, 2), (2, -1), (2, 1)]

result = 0
for step in steps:
    new_row = row + step[0]
    new_col = column + step[1]

    if 1 <= new_row <= 8 and 1 <= new_col <= 8:
        result += 1


print(result)