n = 1260
cnt = 0
coin_types = [500, 100, 50, 10]

for coin in coin_types:
    if n // coin > 0:
        cnt += int(n // coin)
        n %= coin

print(cnt)