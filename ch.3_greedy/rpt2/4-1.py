n = int(input())
trip_plans = list(map(str, input().split()))
x, y = 1, 1

# L, R, U, D
move_types = ['L','R','U','D']
dx = [0, 0, -1, 1]
dy = [-1, 1, 0, 0]


for trip in trip_plans:
    for i in range(len(move_types)):
        if trip == move_types[i]:
            nx = x + dx[i]
            ny = y + dy[i]

    if nx < 1 or ny < 1 or nx > 5 or ny > 5:
        continue

    x, y = nx, ny

print(x, y)
