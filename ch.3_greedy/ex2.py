data = input()

result = int(data[0])

for i in (1, len(data)):
    num = int(data[i])
    if num <= 1 or result <=1:
        result += result
    else:
        result *= result

print(result)