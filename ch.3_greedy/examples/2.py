# 입력값 문자열 s를 리스트화
s = list(input())

result = int(s[0])
for i in range(1, len(s)):
    if int(s[i]) <= 1 or result <= 1:
        result += int(s[i])
    else:
        result *= int(s[i])

print(result)
