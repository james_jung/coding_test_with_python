s = input()
length = len(s)
summary = 0

for i in range(length//2):
    summary += int(s[i])

for i in range(length//2, length):
    summary -= int(s[i])

if summary == 0:
    print("LUCKY")
else:
    print("READY")
