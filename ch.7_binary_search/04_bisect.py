from bisect import bisect_left, bisect_right

def bisect(arr, left, right):
    right_index = bisect_right(arr, right)
    left_index = bisect_left(arr, left)
    return right_index - left_index

    return 


arr = [1,2,4,4,8]
x = 4

print(bisect(arr, 4, 4))
print(bisect(arr, -1, 3)
