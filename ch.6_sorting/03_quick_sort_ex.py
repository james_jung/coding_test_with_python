def quickSort(arr):
    if len(arr) < 1:
        return arr

    pivot = arr[0]
    tail = arr[1:]

    left = [i for i in tail if i < pivot]
    right = [i for i in tail if i > pivot]

    return quickSort(left) + [pivot] + quickSort(right)


arr = [7,5,9,0,3,1,6,2,4,8]

print(quickSort(arr))