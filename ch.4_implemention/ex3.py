place = input()
row = int(place[1])
column = int(ord(place[0])) - (int(ord('a')) - 1)

x, y = row, column

steps =[
    [-2, -1],
    [-2, 1],
    [-1, -2],
    [-1, 2],
    [1, -2],
    [1, 2],
    [2, -1],
    [2, 1]
]

count = 0
for step in steps:
    nx = x + step[0]
    ny = y + step[1]

    if nx >= 1 and ny >= 1 and nx <= 8 and ny <= 8:
        count += 1

print(count)